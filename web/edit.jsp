<%-- 
    Document   : edit
    Created on : May 19, 2019, 11:48:35 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>List Product!</h1>
        <a href="search.jsp">Search</a>
        
        <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="finder" scope="request" />
        
        <form action="ProductEdit" method="POST">
            <input type="hidden" name="id" value="${finder.product.id}">
            
            <label for="name">Name</label>
            <input name="name" id="name" value="${finder.product.name}"><br>
            <label for="description">Description</label>
            <input name="description" id="description" value="${finder.product.desc}"><br>
            
            <input type="submit" value="Update">
        </form>


    </body>
</html>
