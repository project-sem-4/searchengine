/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import com.wpsj.da.ProductDataAcess;
import com.wpsj.entity.Product;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Admin
 */
public class ProductFinderBean {
    private String keyword;
    private int id;
    
    public void setId(int id)
    {
        this.id = id;
    }

    
    public void setKeyword(String keyword)
    {this.keyword = keyword;}
    public List<Product> getProducts(){
    return new 
        ProductDataAcess().getProductsByName(keyword);
            }
    
     public com.wpsj.entity.Product getProduct()
    {
        return new
            com.wpsj.da.ProductDataAcess().getProductById(id);
    }

 
}
