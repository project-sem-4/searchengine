/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ProductDataAcess { 
    
    private PreparedStatement searchStatement;
    
    private PreparedStatement getSearchStatement() throws SQLException, ClassNotFoundException{
       if (searchStatement == null) {
           Connection connection = new DBConnection().getConnection();
           
           searchStatement = connection.prepareStatement("select pro_id, pro_name, pro_desc from ProductStore where pro_name like ?");
       }

        return searchStatement;
    }
    
    public List<Product> getProductsByName(String name){
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%"+name+"%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getInt("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc")));
                
                
            }
            return products;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
   
     private PreparedStatement getSearchByIdStatement() throws ClassNotFoundException,
            SQLException
    {
        if(searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            
            searchStatement = connection.prepareStatement("select id, name, description "
                    + "from Products where id = ?");
        }
        
        return searchStatement;
    }

       private PreparedStatement getUpdateProductStatement() throws ClassNotFoundException,
            SQLException
    {
        if(searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            
            searchStatement = connection.prepareStatement("update Products set name = ?, description = ? "
                    + "where id = ?");
        }
        
        return searchStatement;
    }

        public int updateProduct(int id, String name, String description)
    {
        try {
            PreparedStatement statement = getUpdateProductStatement();
            statement.setString(1, name);            
            statement.setString(2, description);
            statement.setInt(3, id);

            return statement.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
        
           public com.wpsj.entity.Product getProductById(int id)
    {
        try {
            PreparedStatement statement = getSearchByIdStatement();
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                return new com.wpsj.entity.Product(rs.getInt("id"),
                    rs.getString("name"), rs.getString("description"));
            }
            
            return null;
//            return product;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    
    
}
